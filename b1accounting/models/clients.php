<?php

class ModelB1Clients
{

    public static function addClient($b1_client_id, $shop_client_id)
    {
        global $wpdb;
        $wpdb->query($wpdb->prepare("INSERT IGNORE INTO `" . $wpdb->prefix . "b1_clients` (`b1_client_id`, `shop_client_id`) VALUES ('%d' , '%d')",  intval(sanitize_text_field($b1_client_id)),  intval(sanitize_text_field($shop_client_id))));
    }

    public static function getB1Client($shop_client_id)
    {
        global $wpdb;
        return $wpdb->get_results($wpdb->prepare("SELECT * FROM `" . $wpdb->prefix . "b1_clients` WHERE `shop_client_id` =  '%d'", intval(sanitize_text_field($shop_client_id))));
    }

}
