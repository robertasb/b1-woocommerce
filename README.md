Įskiepis skirtas sinchronizuoti produktus tarp WooCommerce ir B1.lt aplikacijos.

### Reikalavimai ###

* PHP 5.5
* WooCommerce 2.6.8
* MySQL 5.7.15

### Diegimas ###

* `NEBŪTINA` Pasidarykite failų atsarginę kopiją.
* Padarykite atsarginę DB kopiją.
* Perkelkite `b1accounting` direktoriją į woocommerce `wp-content/plugins/` direktoriją.
* Administracijos skiltyje įdiekite modulį ir suveskite reikiamą informaciją.
* Administracijos skiltyje 'Orders sync from' laukelyje nurodykite data, nuo kurios bus sinchromnizuojami užsakymai. Datos formatas Y-m-d. Pvz. 2016-12-10 
* Administtracijos skiltyje 'VAT Tax rate ID', nurodykite (jei taikomas PVM) parduotuvės mokėsčio ID. Jos rasite žemiau laukelio.
* Administracijos skiltyje 'Items relations for link' nurodykite prekių susiejimo būdą:
    * One to one - surišama vieną parduotuvės prekė su viena B1 preke, kiekiai yra sinchronizuojami
    * More to one - surišamos keletas parduotuvės prekių su viena B1 preke, kiekiai nėra sinchronizuojami
* Pajunkite cron darbus, 'wp_config.php' faile pridėkite šią eilutę 'define('DISABLE_WP_CRON', true);'
* Prie serverio Cron darbų sąrašo pridėkite šią užduotį:
  Pridėti cron užduotį galite per serverio valdymo panelė (DirectAdmin, Cpanel) arba įvykdę šią komandinę eilutę serverio pusėje
    * `*/5 * * * * wget -q -O - '[cron_url]'` Vietoj [cron_url] reikia nurodyti savo Cron adresą, kuris bus 'http://jusu-domenas.lt/wp-cron.php?doing_wp_cron=1', vietoj 'jusu-domenas.lt', reikia nurodyti Jūsų domeną.
  Cron užduotys galite paleisti ir patys rankiniu būdu modulio konfigūracijos puslapyje, paspausdami atitinkamus mygtukus
* Susiekite B1 ir e.parduotuvės prekės `Nesusiję produktai` skiltyje.
* Įvykdykite cron prekių kiekio sinchronizacijai.
* Norėdami, kad pirkėjai matytų B1 sugeneruotas sąskaitas, reikia `wp-content\plugins\woocommerce\templates\order\order-details.php` faile įterpti norimoje puslapio vietoje nuorodą pvz. 

```
#!php

<?php  
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'b1accounting/b1accounting.php' ) ) {
?>
<a target="_new" href='<?php echo get_query_var( 'b1_accounting_link' ) ?>'>PDF</a>
<?php } ?>

```

### Pastabos ####

Į B1 siunčiami TIK užsakymai su statusu "Completed" (reikšmė 'wc-completed').
Užsakymo data yra laikoma, tą kuri yra nurodyta prie užsakymo e.parduotuvėje. Norint, kad data sutaptų su mokėjimų, prieš patvirtinant užsakymą reikia pakeisti ir šią datą. 

### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt